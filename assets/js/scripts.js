// COOKIES
function controlcookies(){
  localStorage.controlcookie=localStorage.controlcookie||0,localStorage.controlcookie++,cookiesms.style.display='none'
}
localStorage.controlcookie>0&&(document.getElementById('cookiesms').style.bottom='-50px');

// ENLACES OFUSCADOS
document.querySelectorAll('[data-c]').forEach(l => {
  l.addEventListener('click', e => {
    const t = e.currentTarget;
    if (t.hasAttribute('data-t')) {
      window.open(window.atob(t.dataset.c), '_blank');
    } else {
      window.location.href = window.atob(t.dataset.c);
    }
  })
})

// SAFARI SCROLL BEHAVIOR SMOOTH
if (!('scrollBehavior' in document.documentElement.style)) {
  function loadScript (url, callback) {
    if (!document.querySelector(`script[src='${url}']`)) {
      const s = document.createElement('script');
      s.onload = callback;
      s.src = url;
      document.head.appendChild(s);
    }
  }
  function smoothScroll () {
    const links = document.querySelectorAll('[href^="#"]');
    links.forEach(link => {
      link.addEventListener('click', click => {
        click.preventDefault();
        const targetID = link.getAttribute('href');
        const target = document.querySelector(targetID);
        target.scrollIntoView({ behavior: 'smooth' });
        setTimeout(() => {
          window.location.hash = targetID;
        }, 1000)
      })
    })
    if (window.location.hash) {
      // go to top instantaneous
      document.body.scrollIntoView({ behavior: 'auto' });
      // go to has/id smooth
      document.querySelector(window.location.hash).scrollIntoView({ behavior: 'smooth' });
    }
  }
  loadScript('/js/smooth-scroll.js', smoothScroll);
}